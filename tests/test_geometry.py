import pytest
from improc.geometry import Segment
import numpy as np
from skspatial.objects import Point


def test_segment_distance_point():
    assert Segment(Point((1, 3)), Point((3, 4))).distance_point(Point([4, 4])) == 1
    assert Segment(Point((1, 1)), Point((3, 1))).distance_point(Point([2, 2])) == 1
    assert Segment(Point((1, 1)), Point((3, 1))).distance_point(Point([2, 0])) == 1
    assert Segment(Point((1, 1)), Point((3, 1))).distance_point(Point([3, -1])) == 2

    assert Segment(Point((1, 1)), Point((3, 3))).distance_point(Point([1, 0])) == 1
    assert Segment(Point((1, 1)), Point((3, 3))).distance_point(Point([0, 1])) == 1
    assert Segment(Point((1, 1)), Point((3, 3))).distance_point(Point([3, 4])) == 1
    assert Segment(Point((1, 1)), Point((3, 3))).distance_point(Point([4, 3])) == 1

    assert Segment(Point((3, 3)), Point((1, 1))).distance_point(Point([1, 0])) == 1
    assert Segment(Point((3, 3)), Point((1, 1))).distance_point(Point([0, 1])) == 1
    assert Segment(Point((3, 3)), Point((1, 1))).distance_point(Point([3, 4])) == 1
    assert Segment(Point((3, 3)), Point((1, 1))).distance_point(Point([4, 3])) == 1


def test_line_segments():
    assert Segment(Point((1, 3)), Point((3, 4))).distance_segment(Segment(Point((5, 7)), Point((5, 1)))) == 2
    assert Segment(Point((1, 5)), Point((6, 5))).distance_segment(Segment(Point((7, 7)), Point((7, 5)))) == 1

    assert Segment(Point((1, 1)), Point((3, 3))).distance_segment(Segment(Point((0, 4)), Point((1, 3)))) == np.sqrt(2)
    assert Segment(Point((0, 4)), Point((1, 3))).distance_segment(Segment(Point((1, 1)), Point((3, 3)))) == np.sqrt(2)
