import cv2 as cv
import numpy as np
from . import io


def fft(img_cartesian):
    return np.fft.fftshift(np.fft.fft2(img_cartesian))


def imshow_fft(img_freq):
    magnitude_spectrum = 20 * np.log(np.abs(img_freq))
#     cv2.normalize(mag_spec, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U),
    io.imshow(magnitude_spectrum)


def ifft(img_freq):
    # Možná normalize?
    return np.abs(np.fft.ifft2(np.fft.ifftshift(img_freq)))


def rotate_wide(img):
    # This function is a friend with crop_with_contour
    h, w = img.shape[:2]
    if w < h:
        return cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)
    return img


def rotate(img, angle, scale=1):
    '''TODO: new function'''
    resolution = np.asarray(img.shape[1::-1])
    center = resolution / 2
    return cv.warpAffine(img, cv.getRotationMatrix2D(center, -angle, scale), resolution)
