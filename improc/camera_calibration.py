import numpy as np
import cv2 as cv
from pathlib import Path
import yaml
from . import io


def load_camera_matrix(path):
    with open(path, 'r') as f:
        return yaml.load(f, Loader=yaml.Loader)


def save_camera_matrix(path, camera_matrix, dist_coefs):
    Path(path).parent.mkdir(parents=True, exist_ok=True)
    with open(path, 'w') as f:
        data = {
            'camera_matrix': camera_matrix,
            'dist_coefs': dist_coefs
        }
        yaml.dump(data, f)


def create_camera_matrix(path_images, chess_shape=(6, 9), visualize=False):
    '''returns: ret, camera_matrix, dist_coefs, rvecs, tvecs'''
    pts_object = np.hstack([
        np.mgrid[:chess_shape[0], :chess_shape[1]].T.reshape(-1, 2),
        np.zeros((chess_shape[0]*chess_shape[1], 1))
    ]).astype(np.float32)

    imgs_src = io.imread_pattern(path_images)
    imgs_parsed = []
    pts_objects = []  # points in 3D real world space
    pts_images = []  # points in the 2D image space
    for img_src in imgs_src:
        img_grayscale = cv.cvtColor(img_src, cv.COLOR_BGR2GRAY)
        ret, pts_image = cv.findChessboardCorners(img_grayscale, chess_shape)
        if not ret:
            continue
        pts_image = cv.cornerSubPix(img_grayscale, pts_image, (11, 11), (-1, -1),
                                    (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.1))
        pts_images.append(pts_image)
        pts_objects.append(pts_object)

        # Draw and display the corners
        img_parsed = cv.drawChessboardCorners(img_src, chess_shape, pts_image, ret)
        imgs_parsed.append(img_parsed)

    if visualize:
        print(f'{len(imgs_parsed)} from {len(imgs_src)} frames were correctly detected.')
        io.imshow(imgs_parsed[0])
    return cv.calibrateCamera(pts_objects, pts_images, imgs_src[0].shape[1::-1], None, None)
