def hue_to_wavelength(hue):
    # openCV reprezentuje hue uhlem podelenym dvojkou
    assert 0 <= hue and hue <= 180

    if hue > 135:
        # pink/magenta part of a circle
        print("Warning: returned wavelength doesn't correspond")
    return 650 - hue * (650 - 380) / 135
