from matplotlib import pyplot as plt
import numpy as np
import cv2 as cv


def imshow(img):
    # TODO: multiple images

    plt.figure(figsize=(15, 10))
    if img.ndim == 3 and img.dtype == np.uint8:
        plt.imshow(cv.cvtColor(img, cv.COLOR_BGR2RGB))
    elif img.ndim == 2 and img.dtype == np.uint8:  # Grayscale
        plt.imshow(cv.cvtColor(img, cv.COLOR_GRAY2RGB))
    else:
        plt.imshow(img)


def imread_pattern(path):
    vc = cv.VideoCapture(path)
    imgs = []
    ret, img = vc.read()
    while vc.isOpened() and ret:
        imgs.append(img)
        ret, img = vc.read()
    return imgs


def put_text_center(img, text, org, font_face, font_scale, color, thickness=1, line_type=cv.LINE_AA):
    text_size = cv.getTextSize(text, font_face, font_scale, thickness)[0]
    x_pos = org[0] - text_size[0]//2
    y_pos = org[1] + text_size[1]//2
    return cv.putText(img, text, (x_pos, y_pos), font_face, font_scale, color, thickness, line_type)
