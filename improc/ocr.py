import os
import cv2 as cv
from google.cloud import vision
from pytesseract import pytesseract


def google(img, api_key_path='google-vision.json'):
    assert os.path.exists(api_key_path), 'There is no API key in your path!'
    client = vision.ImageAnnotatorClient.from_service_account_json(api_key_path)

    image = vision.Image(content=cv.imencode('.png', img)[1].tobytes())
    response = client.text_detection(image=image)
    annotations = response.text_annotations

    return annotations


def tesseract(img, lang='eng', config='--psm 8'):
    return pytesseract.image_to_string(img, lang, config)
