import numpy as np
import cv2 as cv
from . import io


def crop_with_rect(img, rect):
    (cx, cy), (w, h), rot = rect

    # The first point is the lowest one (y axis), continuing in clockwise direction
    box = cv.boxPoints(rect).astype(np.float32)

    # Now I need to sort box points
    box_upright = np.array([(0, h), (0, 0), (w, 0), (w, h)], dtype=np.float32)
    dsize = np.around(rect[1]).astype(np.int0)

    # Let the transformation begin
    mat = cv.getAffineTransform(box[:3], box_upright[:3])
    return cv.warpAffine(img, mat, dsize)


def crop_with_contour(img, contour):
    return crop_with_rect(img, cv.minAreaRect(contour))


def filter(contours, hierarchy, criterium):
    # OpenCV hierarchy format: [next, previous, first_child, parent]

    contours_res = []
    hierarchy_res = np.array(hierarchy)[0]
    rows_to_stay = []
    rows_to_drop = []
    for idx, (c, h) in enumerate(zip(contours, hierarchy_res)):
        if criterium(c):
            contours_res.append(c)
            rows_to_stay.append(idx)
        else:
            # First, let's sort out first two elements (linked list hierarchy)
            # change next element
            if h[0] != -1:
                hierarchy_res[h[0]][1] = h[1]

            # change previous element
            if h[1] != -1:
                hierarchy_res[h[1]][0] = h[0]

            # And now the last two elements (tree hierarchy)
            # change parent
            if h[3] != -1 and hierarchy_res[h[3]][2] == idx:
                hierarchy_res[h[3]][2] = h[0]

            # update children's parent
            child_idx = h[2]
            while child_idx != -1:
                hierarchy_res[child_idx][3] = h[3]
                child_idx = hierarchy_res[child_idx][0]

            rows_to_drop.append(idx)

    # remove and renumber hierarchy
    map_dict = {curr: new for new, curr in enumerate(rows_to_stay)}
    hierarchy_res = np.delete(hierarchy_res, rows_to_drop, axis=0)
    if hierarchy_res.size != 0:
        hierarchy_res = np.vectorize(lambda x: map_dict.get(x, -1))(hierarchy_res)

    np.testing.assert_equal(len(contours_res), len(hierarchy_res))
    return contours_res, hierarchy_res[np.newaxis, ...]


def hierarchy_get_levels(hierarchy):
    # That hierarchy 3-dimensional array is beyond me...
    hierarchy = hierarchy[0]
    res = np.full(len(hierarchy), -1)

    depth = 0
    current_level = [idx for idx, h in enumerate(hierarchy) if h[3] == -1]
    next_level = []
    while len(current_level) != 0:
        res[current_level] = depth
        for el_curr in current_level:
            el_it = hierarchy[el_curr][2]  # get first child
            while el_it != -1:  # iterate over all children
                next_level.append(el_it)
                el_it = hierarchy[el_it][0]
        current_level = next_level
        next_level = []
        depth += 1

    res[current_level] = depth
    return res


def get_mask_from_contours(img, contours):
    return cv.fillPoly(np.zeros_like(img), contours, 1).astype(bool)


def mask_inside(img, contours, fill_value=0):
    return np.where(get_mask_from_contours(img, contours), img, fill_value)


def mask_outside(img, contours, fill_value=0):
    return np.where(get_mask_from_contours(img, contours), fill_value, img)


def imshow(img, contours, color=(0, 0, 255), thickness=None, *args, **kwargs):
    thickness = 3  # TODO: variable thickness
    img_out = img
    if img.ndim == 2:
        img_out = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
    cv.drawContours(img_out, contours, -1, color, thickness, *args, **kwargs)
    io.imshow(img_out)
