from . import camera_calibration
from . import contour
from . import geometry
from . import io
from . import jupyter
from . import ocr
from . import other
from . import shape
from . import transform


__all__ = [
    'camera_calibration',
    'contour',
    'geometry',
    'io',
    'jupyter',
    'ocr',
    'other',
    'shape',
    'transform',
]

from . import _version
__version__ = _version.get_versions()['version']
