import cv2 as cv
import numpy as np


def aspect_ratio(contour):
    (cx, cy), (w, h), rotation = cv.minAreaRect(contour)
    return w / h


def solidity(contour):
    return cv.contourArea(contour) / cv.contourArea(cv.convexHull(contour))


def extent(contour):
    min_rect = cv.minAreaRect(contour)
    min_box = np.int0(cv.boxPoints(min_rect))
    return cv.contourArea(contour) / cv.contourArea(min_box)


def equivalent_diameter(contour):
    area = cv.contourArea(contour)
    return np.sqrt(4*area/np.pi)
