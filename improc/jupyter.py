from IPython.display import HTML, display
import ipywidgets as widgets
import glob


def imshow(path, cols=2):
    images = sorted(glob.glob(path))
    width = 98 / cols
    html_code = '<div class="row">'
    for image_path in images:
        html_code += f'<img src={image_path} class="preview" style="width: {width}%"></img>'
    html_code += '</div>'
    display(HTML(html_code))


def create_slider(*args, **kwargs):
    return widgets.IntSlider(
        *args, **kwargs,
        continuous_update=False,
        layout=widgets.Layout(width='auto', grid_area='header')
    )


def create_range_slider(*args, **kwargs):
    return widgets.IntRangeSlider(
        *args, **kwargs,
        continuous_update=False,
        layout=widgets.Layout(width='auto', grid_area='header')
    )
