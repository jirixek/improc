from __future__ import annotations
import numpy as np
from skspatial.objects import Line, Point, Vector


# TODO: i don't like key parameter
# TODO: mohlo by to fungovat s numpy array
def sort_clockwise(pts, key=lambda x: x):
    pts_sorted_y = sorted(pts, key=lambda x: key(x)[0])

    n = len(pts)
    pts_top = pts_sorted_y[:n//2]
    pts_bottom = pts_sorted_y[n//2:]

    pts_top.sort(key=lambda x: key(x)[1])
    pts_bottom.sort(reverse=True, key=lambda x: key(x)[1])

    return pts_top + pts_bottom


def measure_accuracy(object_size, resolution):
    # 5% fuga na kazdou stranu
    resolution_ratio = resolution[0] / resolution[1]
    object_ratio = object_size[0] / object_size[1]

    if resolution_ratio < object_ratio:
        size = object_size[0]
        px = resolution[0]
    else:
        size = object_size[1]
        px = resolution[1]
    return size * 1.1 / px


def px_mm_coef(dist_px, dist_mm):
    return dist_px / dist_mm  # number of pixels in one millimeter


def px_to_mm(dist_px, k):
    return np.asarray(dist_px) / k


def mm_to_px(dist_mm, k):
    return np.asarray(dist_mm) * k


# TODO: separatni file?
class Segment():
    def __init__(self, point_a: Point, point_b: Point):
        self.point_a = point_a
        self.point_b = point_b

    def to_line(self) -> Line:
        return Line.from_points(self.point_b, self.point_a)

    def shortest_path_point(self, point: Point) -> Point:
        vec_segment = Vector.from_points(self.point_a, self.point_b)
        vec_perpendicular = np.array([vec_segment[1], -vec_segment[0]])
        assert vec_segment.is_perpendicular(vec_perpendicular)
        vec_segment_coef, vec_perpendicular_coef = Segment.__line_coefs(
            self.to_line(), Line.from_points(point, point + vec_perpendicular))

        if 0 <= vec_segment_coef <= 1:
            return Point(point + vec_perpendicular_coef * vec_perpendicular)
        else:
            d1 = point.distance_point(self.point_a)
            d2 = point.distance_point(self.point_b)
            if d1 < d2:
                return self.point_a
            else:
                return self.point_b

    def shortest_path_segment(self, other: Segment) -> Segment:
        if self.is_intersecting(other):
            intersection = self.to_line().intersect_line(other.to_line())
            return Segment(intersection, intersection)

        choices = [
            (other.point_a, self.shortest_path_point(other.point_a)),
            (other.point_b, self.shortest_path_point(other.point_b)),
            (self.point_a, other.shortest_path_point(self.point_a)),
            (self.point_b, other.shortest_path_point(self.point_b)),
        ]
        return min([Segment(point_a, point_b) for point_a, point_b in choices], key=Segment.norm)

    def norm(self) -> np.float64:
        return np.linalg.norm(self.point_b - self.point_a)

    def distance_point(self, point: Point) -> np.float64:
        return Segment(self.shortest_path_point(point), point).norm()

    def distance_segment(self, other: Segment) -> np.float64:
        return self.shortest_path_segment(other).norm()

    def is_intersecting(self, other: Segment) -> bool:
        self_line, other_line = self.to_line(), other.to_line()
        self_sides = [self_line.side_point(other.point_a), self_line.side_point(other.point_b)]
        other_sides = [other_line.side_point(self.point_a), other_line.side_point(self.point_b)]
        # We'll consider improper intersection (segments touching) as an intersection
        return self_sides[0] != self_sides[1] and other_sides[0] != other_sides[1]

    @staticmethod
    def __line_coefs(line_a: Line, line_b: Line):
        # pt1 + a*vec1 = pt2 + b*vec2
        pt_diff = line_b.point - line_a.point
        vec_cross = np.cross(line_a.direction, line_b.direction)
        if vec_cross == 0:
            if (pt_diff == 0).all():
                raise ValueError('Lines are colinear')
            else:
                raise ValueError('Lines do not intersect')

        a = np.cross(pt_diff, line_b.direction) / vec_cross
        b = np.cross(pt_diff, line_a.direction) / vec_cross
        return a.item(), b.item()
